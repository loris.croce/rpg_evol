# Population Model

Model of the dynamics of population for French farmers that includes two
  processes, an installation process and a retirement process.
The population is represented by an array of age class amounts.
The installation process is modeled by a beta distribution with 4 parameters:
  amplitude, drop_x, mu and v. The retirement process is a linear function
  with a threshold.

See documentation in the notebook "ABC Calibration Yearly Model.ipynb"

## Usage

First, you need to calibrate the model with the script abc.R. You can adjust the number
of samples `nb_simul`, then run:
```
Rscript abc.R
```

Then you can use the Python API exposed by `population_model.py` for using the calibrated parameters and computing the yearly dynamics.
