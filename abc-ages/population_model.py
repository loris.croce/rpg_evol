#!/usr/bin/python3
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import scipy
from scipy import stats

class PopulationModel():
    '''
    Model of the dynamics of population for French farmers that includes two
     processes, an installation process and a retirement process.
    The population is represented by an array of age class amounts.
    The installation process is modeled by a beta distribution with 4 parameters:
     amplitude, drop_x, mu and v. The retirement process is a linear function
     with a threshold.
    See documentation in the notebook "ABC Calibration Daily Model.ipynb".
    '''
    def __init__(self, ageclasses, params_filename, weights_filename, nbsamples=1):
        '''
        Initialize an instance of the model
        :param ageclasses inital data of age classes amounts
        :param params_filename filename of the posteriors of the calibrated
        parameters
        :param weights_filename filename of the weights for this posteriors
        :param nbsamples number of samples that will be handled by this instance
        '''
        self.ageclasses = np.repeat([ageclasses], nbsamples, axis=0)
        params = pd.read_csv(params_filename)
        weights = pd.read_csv(weights_filename).iloc[:,0]
        kernel = stats.gaussian_kde(params.transpose(), weights=weights)
        self.parameters = kernel.resample(nbsamples).transpose()

    def step_year(self):
        '''
        Compute the evolution of the age classes amounts for one year. The
        new amounts will be stored in the instance, ready for a next step, and
        will also be returned.
        '''
        # First, we shift the amounts by one year, and affect 0 for the
        # new ageclass
        self.ageclasses = np.roll(self.ageclasses,1)
        self.ageclasses[:,0] = 0
        # Now, we process every parameters set sampled
        for i in range(len(self.ageclasses)):
            [amplitude,drop_x,mu,v,r_threshold,r_factor] = self.parameters[i,:]
            alpha = mu * v
            beta = (1 - mu) * v
            newages = self.ageclasses[i,:]
            newages = map(lambda elt:
                      elt[1]
                      + amplitude*stats.beta.pdf((1+elt[0])/drop_x, alpha, beta)
                       - 0 if ((1+elt[0]) < r_threshold) else elt[1]*r_factor
                      , enumerate(newages))
            self.ageclasses[i,:] = list(newages)
        return self.ageclasses

if __name__ == '__main__':
    ages_msa_2008 = [5, 47, 194, 529, 1056, 1760, 2477, 3184, 4013, 4617, 5210, 5495, 5981, 6354, 6949, 7646, 8706, 9973, 10766, 11644, 12479, 13180, 13736, 14781, 15885, 16682, 17803, 17706, 17987, 18386, 18158, 17949, 17533, 18011, 17308, 17259, 17538, 18032, 18606, 17364, 16668, 15279, 13731, 7604, 5294, 3282, 2669, 2286, 1614, 1236, 1007, 1074, 978, 829, 789, 655, 697, 591, 496, 471, 403, 348, 323, 269, 248, 240, 220, 171, 178, 201, 248, 130, 58, 63, 43, 42, 55, 42, 30, 30, 9, 10, 15]
    model = PopulationModel(ages_msa_2008,"../out/abc_10000.Rdata_param.csv", "../out/abc_10000.Rdata_weights.csv",nbsamples=1)
    for i in range(10):
        target = model.step_year()
    data=pd.DataFrame(target)
    # print(data.head())
    data.to_csv("../out/data.csv")
