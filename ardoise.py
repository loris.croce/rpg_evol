import pandas as pd
from numpy import random
# Build cars DataFrame
names = ['United States', 'Australia', 'Japan', 'India', 'Russia', 'Morocco', 'Egypt']
dr =  [True, False, False, False, True, True, True]
cpc = [809, 731, 588, 18, 200, 70, 45]
lst = [random.randint(10, size=random.randint(1,5)) for i in range(len(names))]
dict = { 'country':names, 'cars_per_cap':cpc, "lst": lst}
cars = pd.DataFrame(dict)
# print(cars)

# Definition of row_labels
row_labels = ['US', 'AUS', 'JAP', 'IN', 'RU', 'MOR', 'EG']

# Specify row labels of cars
cars.index = row_labels

# Print cars again
cars_sample = cars.sample(3)
cars_sample["country"] = "foo"
cars_sample["lst"] = cars_sample["lst"].apply(lambda x: [5 for i in range(len(x))])
print(cars)
print("---")
print(cars_sample)
cars = cars.drop(list(cars_sample.index.values))
print(cars)
print("---")
cars = pd.concat([cars_sample, cars], axis=0)
print(cars)