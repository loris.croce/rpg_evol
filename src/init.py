import geopandas as gpd
import matplotlib.pyplot as plt
import sys
import os

def create_rpg_pra():
    # PRA
    pra = gpd.read_file("data/pra/pra.shp")
    pra.geometry = pra.geometry.to_crs(2154)
    pra = pra.drop([716,717,718,719,720])
    # REGIONS
    reg = gpd.read_file("data/Regions2016/regions-20161121.shp")
    reg.geometry = reg.geometry.to_crs(2154)
    reg = reg.drop([0,1,2,3,4])
    # RPG
    rpg = gpd.read_file("data/RPG_id/l_ilot_anonyme_groupe_dominant_s_r84_2016.shp")
    # AURA
    aura = reg[reg.code_insee.eq("84")]
    aura.loc[15].geometry
    pra_aura = pra[pra.geometry.centroid.within(aura.loc[15].geometry)]
    # RAM cleaning
    del reg
    del aura
    del pra
    # Join rpg/pra
    rpg_pra = gpd.tools.sjoin(rpg, pra_aura, op='within', how='left')
    # RAM cleaning
    del rpg
    del pra_aura
    # Export
    rpg_pra.to_file("results/rpg_id_pra.shp", driver="ESRI Shapefile")
    del rpg_pra