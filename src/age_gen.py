import numpy as np
import pandas as pd
import geopandas as gpd
import os, glob
import matplotlib.pyplot as plt
import pyproj
import shapely.ops
import shapely.geometry
from functools import partial
import sys

ages_hist = [1, 76, 268, 579, 1079, 1493, 1973, 2432, 3059, 3592, 4236, 4717, 5440, 5904, 6419, 6945, 7574, 7847, 8414, 8704, 8992, 8782, 8821, 9069, 9236, 9741, 10569, 11458, 12058, 12623, 12927, 13351, 13637, 14446, 15461, 16145, 17113, 17017, 17163, 17660, 17350, 17190, 16147, 12294, 10037, 6746, 5509, 4670, 3720, 2830, 2401, 2181, 1856, 1584, 1209, 790, 665, 595, 496, 390, 333, 346, 303, 246, 238, 205, 201, 175, 146, 125, 92, 82, 75, 55, 50, 36, 36, 26, 25, 18, 13, 10, 11] # msa 2018
ages = np.arange(18,101)
probas = [i/sum(ages_hist) for i in ages_hist]

exploits = pd.read_csv("results/exploits.csv", sep=";")
exploits["age"] = np.random.choice(range(len(ages)), size=len(exploits), p=probas) + 18
if not os.path.exists("results/exploits_age"):
    os.makedirs("results/exploits_age")
exploits.to_csv("results/exploits_age/exploits" + sys.argv[1] + ".csv", sep=";")
